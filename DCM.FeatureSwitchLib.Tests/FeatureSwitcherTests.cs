﻿using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;

namespace DCM.FeatureSwitchLib.Tests
{
    [TestFixture]
    public class FeatureSwitcherTests
    {
        private IFeatureSwitcher _featureSwitcher;

        [SetUp]
        public void Init()
        {
            var storageProvider = Substitute.For<IFeatureSwitchStorageProvider>();
            storageProvider.RetrieveFeatureSwitches().Returns(new List<string>());

            _featureSwitcher = new FeatureSwitcher(storageProvider, "");
        }
    }
}
