﻿using System.Collections.Generic;

namespace DCM.FeatureSwitchLib
{
    public class FeatureSwitch
    {
        public string Name { get; set; }

        public Dictionary<string, string> Properties { get; set; }

        public FeatureSwitch()
        {
            Properties = new Dictionary<string, string>();
        }
    }
}