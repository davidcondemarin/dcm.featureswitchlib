﻿namespace DCM.FeatureSwitchLib
{
    public interface IFeatureSwitcher
    {
        char PropertySeparator { get; }

        void RegisterPropertyReader(string propertyRouteName, IFeaturePropertyReader reader);

        bool IsFeatureEnabled(string featureName, bool defaultValue = false);
    }
}