﻿using System;

namespace DCM.FeatureSwitchLib
{
    public interface IFeaturePropertyReader
    {
        string CurrentValue();
    }
}
