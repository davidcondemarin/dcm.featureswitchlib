﻿using System;
using System.Collections.Generic;

namespace DCM.FeatureSwitchLib
{
    public class FeatureSwitcher : IFeatureSwitcher
    {
        private readonly IFeatureSwitchStorageProvider _storageProvider;

        private readonly string _featureSwitchPattern;

        private Dictionary<string, IFeaturePropertyReader> _featurePropertyReaders;

        public FeatureSwitcher(IFeatureSwitchStorageProvider storageProvider, string featureSwitchPattern)
        {
            _storageProvider = storageProvider ?? throw new InvalidOperationException("Storage Provider cannot be null");
            _featureSwitchPattern = featureSwitchPattern;
            _featurePropertyReaders = new Dictionary<string, IFeaturePropertyReader>();
        }

        public char PropertySeparator => '_';

        public void RegisterPropertyReader(string propertyRouteName, IFeaturePropertyReader reader)
        {
            throw new System.NotImplementedException();
        }

        public bool IsFeatureEnabled(string featureName, bool defaultValue = false)
        {
            throw new NotImplementedException();
        }
    }
}