﻿using System.Collections.Generic;

namespace DCM.FeatureSwitchLib
{
    public interface IFeatureSwitchStorageProvider
    {
        IEnumerable<string> RetrieveFeatureSwitches();
    }
}