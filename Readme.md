﻿# Feature Switch Library

## Initial features:

 - Global feature switches
 - Feature switch configurator
   - Per environment
   - Per running boxW

```{property1}_{property2}_{feature}```
 
### Sample feature switch

 MYBOXID_LIVE_TurnOnFooBar

```
var fsService = new FeatureSwitcher(new FeatureSwitchSource(), "{environment}_{server}_{name}");
fsService.RegisterPropertyReader("environment", new CurrentEnvironmentReader());
fsService.RegisterPropertyReader("server", new CurrentEnvironmentReader());
```